require "test_helper"

class KvkuhrPalindromeTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::KvkuhrPalindrome::VERSION
  end

  def test_non_palindrome
    refute "apple".palindrome?
  end

  def test_literal_palindrome
    assert "racecar".palindrome?
  end

  def test_mixed_case_palindrome
    assert "RaceCar".palindrome?
  end

  def test_palindrome_punctuated
    assert "A man, a plan, a canal - Panama!".palindrome?
  end


end
